package test.cases.jira;

import com.telerikacademy.testframework.Utils;
import net.bytebuddy.build.Plugin;
import org.apache.logging.log4j.core.config.Order;
import org.apache.logging.log4j.core.net.Priority;
import org.junit.Assert;
import org.junit.Test;


import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import pages.jira.JiraLoginPage;

import java.util.List;

import static com.telerikacademy.testframework.Data.Constants.*;
import static com.telerikacademy.testframework.UserActions.loadBrowser;
import static com.telerikacademy.testframework.Utils.getUIMappingByKey;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.replace;



    public class CreateIssue extends BaseTest{
        String currentIssueKey, tempIssueKey, key="";

        @Test
        public void createStory(){

            JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver());
            loginPage.loginUser("user");
            actions.waitFor(10000);

            createIssue("Story","Low");
        }

        @Test
        public void createBug(){

//        JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver());
//        loginPage.loginUser("user");
            actions.waitFor(5000);
            createIssue("Bug","High");

        }


}
