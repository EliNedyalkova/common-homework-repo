package test.cases.jira;

import com.telerikacademy.testframework.Utils;
import net.bytebuddy.build.Plugin;
import org.apache.logging.log4j.core.config.Order;
import org.apache.logging.log4j.core.net.Priority;
import org.junit.Assert;
import org.junit.Test;


import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import pages.jira.JiraLoginPage;

import java.util.List;

import static com.telerikacademy.testframework.Data.Constants.*;
import static com.telerikacademy.testframework.UserActions.loadBrowser;
import static com.telerikacademy.testframework.Utils.getUIMappingByKey;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.replace;


public class JiraTests extends BaseTest{
    String currentIssueKey, tempIssueKey, key="";

    @Test
    public void createStory(){

        JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver());
        loginPage.loginUser("user");
        actions.waitFor(10000);

        createIssue("Story","Low");
   }

    @Test
    public void createBug(){

//        JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver());
//        loginPage.loginUser("user");
        actions.waitFor(5000);
        createIssue("Bug","High");

    }


    @Test
    public void linkBugAndStoryTest(){


//        JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver());
//        loginPage.loginUser("user");
        actions.waitFor(10000);
        Utils.getWebDriver().get(format(Utils.getConfigPropertyByKey("jira.accountUrl")+"browse/"+storyId));

        actions.waitFor(5000);
        actions.waitForElementVisible("jira.link.issueButton");
        WebElement linkIssueButton=actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.link.issueButton")));
        linkIssueButton.click();

        actions.waitForElementVisible("jira.link.issueSearch");
        WebElement linkIssueSearch=actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.link.issueSearch")));
        linkIssueSearch.click();

        actions.waitFor(5000);
        WebElement bugSelected=actions.getDriver().findElement(By.xpath("//input[@id='issue-link-search']"));

        bugSelected.sendKeys(bugId + Keys.ENTER);

        actions.waitFor(15000);
        actions.waitForElementVisible("jira.confirmLinkButton");
        WebElement confirmLinkButton = actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.confirmLinkButton")));

        confirmLinkButton.click();
        loadBrowser("jira.loginUrl");
    }

}
