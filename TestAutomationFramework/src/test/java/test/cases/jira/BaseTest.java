package test.cases.jira;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.jira.JiraLoginPage;

import java.util.List;

import static com.telerikacademy.testframework.Data.Constants.*;
import static com.telerikacademy.testframework.UserActions.loadBrowser;
import static com.telerikacademy.testframework.Utils.getUIMappingByKey;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.replace;

public class BaseTest {

    UserActions actions = new UserActions();

    @BeforeClass

    public static void setup() {
        UserActions.loadBrowser("jira.loginUrl");

    }

    @AfterClass
        public static void tearDown(){ UserActions.quitDriver(); }

    public String getLastIssueKey () {
       String tempKey="";
        WebElement element= actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.lastIssueKey")));
        tempKey=element.getText();
        tempKey=replace(tempKey, "\n", "");
        return tempKey;
    }

    public String getExpectedIssueKey (String currentKey) {
        String newKeyNumber,tempKey;
        tempKey=currentKey;

        String[] partsTempKey = tempKey.split("-");
        newKeyNumber=String.valueOf(Integer.parseInt(partsTempKey[1])+1);
        tempKey=replace(tempKey,partsTempKey[1],newKeyNumber);

        return tempKey;
    }

    public void createIssue(String type, String priority){

        String summaryIssue, descriptionIssue;
        String currentIssueKey,tempIssueKey;

        if (type=="Story"){

            summaryIssue=STORY_SUMMARY;
            descriptionIssue=STORY_DESCRIPTION;

        } else {
            summaryIssue=BUG_SUMMARY;
            descriptionIssue=BUG_DESCRIPTION;
        }

        lastIssueId=getLastIssueKey();

        actions.waitForElementVisible("jira.createIssueButton");
        actions.clickElement("jira.createIssueButton");

        actions.waitForElementVisible("jira.issueTypeInput");
        WebElement issueTypeInput =actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.issueTypeInput")));
        issueTypeInput.click();

        WebElement issueType =actions.getDriver().findElement(By.xpath(getUIMappingByKey(format("jira.issueType"+type))));
        issueType.click();

        actions.waitForElementVisible("jira.summary.input");
        WebElement summary=actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.summary.input")));
        summary.sendKeys(summaryIssue);

        actions.waitFor(5000);

        actions.waitForElementVisible("jira.description.input");
        WebElement description=actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.description.input")));
        description.sendKeys(descriptionIssue);

        List<WebElement> dropDownList = actions.getDriver().findElements(By.xpath(getUIMappingByKey("jira.priorityInput")));
        dropDownList.get(2).click();

        WebElement priorityType =actions.getDriver().findElement(By.
                xpath(getUIMappingByKey(format("jira.priorityType"+priority))));
        priorityType.click();

        WebElement confirmCreateIssueButton = actions.getDriver().findElement
                (By.xpath(getUIMappingByKey("jira.confirmCreateIssueButton")));
        confirmCreateIssueButton.click();

        loadBrowser("jira.loginUrl");

        currentIssueKey=getLastIssueKey();
        tempIssueKey=getExpectedIssueKey(lastIssueId);

        Assert.assertEquals(format("%s is not created.",type),tempIssueKey,currentIssueKey);

        WebElement element= actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.lastIssueSummary")));
        Assert.assertEquals("Summary is not as expected.",summaryIssue,element.getText());

        element= actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.lastIssueType")));
        Assert.assertEquals("Summary is not as expected.",type,element.getAttribute("alt"));

        if (type=="Story") {
            storyId = currentIssueKey;
        }else {
            bugId = currentIssueKey;
        }
    }
}
