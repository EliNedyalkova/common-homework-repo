package test.cases.jira;

import com.telerikacademy.testframework.Utils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import static com.telerikacademy.testframework.Data.Constants.bugId;
import static com.telerikacademy.testframework.Data.Constants.storyId;
import static com.telerikacademy.testframework.UserActions.loadBrowser;
import static com.telerikacademy.testframework.Utils.getUIMappingByKey;
import static java.lang.String.format;

public class LinkIssues  extends BaseTest{

    @Test
    public void linkBugAndStoryTest(){


//        JiraLoginPage loginPage = new JiraLoginPage(actions.getDriver());
//        loginPage.loginUser("user");
        actions.waitFor(10000);
        Utils.getWebDriver().get(format(Utils.getConfigPropertyByKey("jira.accountUrl")+"browse/"+storyId));

        actions.waitFor(5000);
        actions.waitForElementVisible("jira.link.issueButton");
        WebElement linkIssueButton=actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.link.issueButton")));
        linkIssueButton.click();

        actions.waitForElementVisible("jira.link.issueSearch");
        WebElement linkIssueSearch=actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.link.issueSearch")));
        linkIssueSearch.click();

        actions.waitFor(5000);
        WebElement bugSelected=actions.getDriver().findElement(By.xpath("//input[@id='issue-link-search']"));

        bugSelected.sendKeys(bugId + Keys.ENTER);

        actions.waitFor(15000);
        actions.waitForElementVisible("jira.confirmLinkButton");
        WebElement confirmLinkButton = actions.getDriver().findElement(By.xpath(getUIMappingByKey("jira.confirmLinkButton")));

        confirmLinkButton.click();
        loadBrowser("jira.loginUrl");
    }

}
