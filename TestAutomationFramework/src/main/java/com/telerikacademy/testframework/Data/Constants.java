package com.telerikacademy.testframework.Data;

public class Constants {

    public static String storyId="";
    public static String bugId="";
    public static String lastIssueId="";

    public static final String STORY_SUMMARY="Display Company Info when Switch Languages";
    public static final String STORY_DESCRIPTION="As a user of travel agency’s website\n" +
            "if I choose English Language from drop-down list\n" +
            "Company Info is displayed.\n" +
            "\n" +
            "Steps to reproduce:\n" +
            "1. Navigate to main page https://www.phptravels.net/\n" +
            "2. From drop-down list choose English Language\n" +
            "3. Navigate to About Us choice and select it\n" +
            "\n" +
            "expected result: Company Info is displayed in English \n" +
            "actual result: Company Info is displayed in Englis.";
    public static final String STORY_PRIORITY="Low";

    public static final String BUG_SUMMARY="Display Company Info when Switch Language to German";
    public static final String BUG_DESCRIPTION="As a user of travel agency’s website\n" +
            "if I choose German Language from drop-down list\n" +
            "Company Info is not displayed.\n" +
            "\n" +
            "Steps to reproduce:\n" +
            "1. Navigate to main page https://www.phptravels.net/\n" +
            "2. From drop-down list choose German Language\n" +
            "3. Navigate to About Us choice and select it\n" +
            "\n" +
            "expected result: Company Info is displayed in German \n" +
            "actual result: No Company Info is displayed.";
    public static final String BUG_PRIORITY="High";

}
