package pages.trello;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BaseTrelloPage {

    public LoginPage(WebDriver driver) {
        super(driver, "trello.loginUrl");
    }

    public void loginUser(String userKey) {
        String username = Utils.getConfigPropertyByKey("trello.users." + userKey + ".username");
        String password = Utils.getConfigPropertyByKey("trello.users." + userKey + ".password");

        navigateToPage();

        actions.waitForElementVisible("trello.loginPage.username");
        actions.typeValueInField(username, "trello.loginPage.username");
        actions.clickElement("trello.loginPage.loginButton");

        actions.waitFor(2000);

        actions.waitForElementVisible("trello.loginPage.password");
        actions.typeValueInField(password, "trello.loginPage.password");
        actions.clickElement("trello.loginPage.loginButton");
    }
}
