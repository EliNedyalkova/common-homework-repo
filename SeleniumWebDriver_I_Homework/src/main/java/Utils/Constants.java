package Utils;


public class Constants {
// Common constants
    public static final String WEBDRIVER = "webdriver.chrome.driver";
    public static final String WEBDRIVER_SOURCE =
            "C:\\Users\\Eli\\Documents\\Training\\_Applications\\chromedriver_win32\\chromedriver.exe";
    public static final String TARGET_SEARCH = "Telerik Academy Alpha";
    public static final String TITLE_EXPECTED_RESULT = "IT Career Start in 6 Months - Telerik Academy Alpha";

// Bing constants
    public static final String BING_UNDER_TEST = "https://www.bing.com";
    public static final String INPUT_ELEMENT_ID_BING = "sb_form_q";
    public static final String SEARCH_RESULT_XPATH_BING = "//h2[@class=' b_topTitle']/a";

    // Google constants
    public static final String GOOGLE_UNDER_TEST = "https://www.google.com";
    public static final String INPUT_ELEMENT_ID_GOOGLE = "q";
    public static final String ACCEPT_BUTTON_XPATH_GOOGLE = "//button[@id='L2AGLb']";
    public static final String SEARCH_RESULT_XPATH_GOOGLE = "//h3[@class='LC20lb MBeuO DKV0Md']";

}
