import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import static Utils.Constants.*;

public class TestGoogle {

    @Test
    public void validateListElementWithGoogle() {

        System.setProperty(WEBDRIVER, WEBDRIVER_SOURCE);
        WebDriver webDriver = new ChromeDriver();
        webDriver.get(GOOGLE_UNDER_TEST);

        WebElement elementAccept = webDriver.findElement(By.xpath(ACCEPT_BUTTON_XPATH_GOOGLE));
        new Actions(webDriver).moveToElement(elementAccept)
                .click()
                .perform();

        WebElement element = webDriver.findElement(By.name(INPUT_ELEMENT_ID_GOOGLE));
        element.sendKeys(TARGET_SEARCH);
        element.submit();

        WebElement searchResult = webDriver.findElement(By.xpath(SEARCH_RESULT_XPATH_GOOGLE));

        Assert.assertTrue("The first position is not as expected",(searchResult.getText())
                .equals(TITLE_EXPECTED_RESULT));

        webDriver.quit();
    }
}
