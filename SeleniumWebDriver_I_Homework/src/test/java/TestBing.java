
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static Utils.Constants.*;


public class TestBing {

    @Test
    public void validateListElementWithBing() {

        System.setProperty(WEBDRIVER, WEBDRIVER_SOURCE);
        WebDriver webDriver = new ChromeDriver();
        webDriver.get(BING_UNDER_TEST);

        WebElement element = webDriver.findElement(By.id(INPUT_ELEMENT_ID_BING));
        element.sendKeys(TARGET_SEARCH);
        element.submit();

        WebElement searchResult = webDriver.findElement(By.xpath(SEARCH_RESULT_XPATH_BING));

        Assert.assertTrue("The first position is not as expected",(searchResult.getAttribute("text"))
                .equals(TITLE_EXPECTED_RESULT));

        webDriver.quit();

    }
}
